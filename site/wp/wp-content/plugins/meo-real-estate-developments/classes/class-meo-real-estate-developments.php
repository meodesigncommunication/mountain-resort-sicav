<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoRealEstateDevelopments {

	private $dao;
	private $translator;
	private $smartcapture;


	public function __construct() {
		require_once( dirname(__FILE__) . '/class-development-dao-p2p.php');
		require_once( dirname(__FILE__) . '/class-wordpress-translator.php');
		require_once( dirname(__FILE__) . '/class-meo-smart-capture-api.php');
		$this->dao = new DevelopmentDaoP2P();
		$this->translator = new WordPressTranslator();
		$this->smartcapture = new MeoSmartCaptureApi($this);
	}

	public static function activate() {
		// Do nothing
	}

	public static function deactivate() {
		// Do nothing
	}


	public function reset() {
		$this->dao->reset();
		$this->dao = new DevelopmentDaoP2P();
		return true;
	}

	public function getDevelopments($ids = array()) {
		return $this->dao->getDevelopments($ids);
	}

	public function getSectors($ids = array()) {
		return $this->dao->getSectors($ids);
	}

	public function getBuildings($ids = array()) {
		return $this->dao->getBuildings($ids);
	}

	public function getFloors($ids = array()) {
		return $this->dao->getFloors($ids);
	}

	public function getLots($ids = array()) {
		return $this->dao->getLots($ids);
	}

	public function getPlans($ids = array()) {
		return $this->dao->getPlans($ids);
	}

	public function getEntries($ids = array()) {
		return $this->dao->getEntries($ids);
	}

	public function getLotTypes() {
		return $this->dao->getLotTypes();
	}

	public function translate($phrase, $language = null) {
		return $this->translator->translate($phrase, $language);
	}

	public function getAjaxLotList() {
		$this->smartcapture->getAjaxLotList();
	}

	public function getAjaxContactList() {
		$this->smartcapture->getAjaxContactList();
	}

	public function getAjaxContact() {
		$this->smartcapture->getAjaxContact();
	}

	public function getAjaxDevelopment() {
		$this->smartcapture->getAjaxDevelopment();
	}

	public function getAjaxStatuses() {
		$this->smartcapture->getAjaxStatuses();
	}

	public function setAjaxStatus() {
		$this->smartcapture->setAjaxStatus();
	}

	public function setAjaxPrice() {
		$this->smartcapture->setAjaxPrice();
	}
}
