<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoSmartCaptureApi {

	private $meoRealEstateDevelopments;


	public function __construct($meoRealEstateDevelopments) {
		$this->meoRealEstateDevelopments = $meoRealEstateDevelopments;
	}

	public function getAjaxLotList() {
		$this->sendOriginHeaders();
		header('Content-Type: application/json');

		$buildings = $this->meoRealEstateDevelopments->getBuildings();
		$floors = $this->meoRealEstateDevelopments->getFloors();
		$lots = $this->meoRealEstateDevelopments->getlots();

		$result = array(
			"id" => -1,
			"fieldErrors" => array(),
			"sError" => "",
			"aaData" => array()
		);

		$row = 1;
		foreach( $lots as $lot ) {
			$floor = $floors[$lot['floor_id']];
			$building = $buildings[$floor['building_id']];

			$result["aaData"][] = (object) array(
				"DT_RowId" => "row_" . $lot['id'],
				"lot_type" => $lot['type']['name'],
				"building" => $building['code'],
				"floor" => $floor['ordinal'],
				"lot" => $lot['code'],
				"rooms" => $lot['pieces'],
				"surface" => $lot['surface_weighted'],
				"availability" => $lot['availability'],
				"price" => $lot['price'],
				"lot_id" => $lot['id']
			);
			$row++;
		}

		echo json_encode($result);
		exit;
	}

	public function getAjaxContactList() {
		$this->sendOriginHeaders();
		header('Content-Type: application/json');

		$this->checkApiKey();

		$result = array(
			"id" => -1,
			"fieldErrors" => array(),
			"sError" => "",
			"aaData" => array()
		);


		$entries = $this->getDownloads();


		$entries_by_email = array();
		
		foreach( $entries as $entry_index => &$entry ) {
			
			if (!isset($entries_by_email[$entry['email']])) {
				
				$entries_by_email[$entry['email']] = array(
					'submit_time'  => $entry['submit_time'],
					'surname'      => $entry['surname'],
					'first_name'   => $entry['first_name'],
					'address'      => $entry['address'],
					'postcode'     => $entry['postcode'],
					'city'         => $entry['city'],
					'country'      => $entry['country'],
					'email'        => $entry['email'],
					'phone'        => $entry['phone'],
					'language'     => $entry['language'],
					'contact_type' => $entry['contact_type'],
					'lot_code'     => array(),
					'analytics_id' => array()
				);
			}

			if ((float)$entry['submit_time'] < (float)$entries_by_email[$entry['email']]['submit_time']) {
				
				$entries_by_email[$entry['email']]['submit_time'] = $entry['submit_time'];
			}

			foreach (array('lot_code', 'analytics_id') as $field) {
				if (isset($entry[$field]) && !empty($entry[$field]) && !in_array($entry[$field], $entries_by_email[$entry['email']][$field])) {
					
					$entries_by_email[$entry['email']][$field][] = $entry[$field];
				}
			}
			
			unset($entries[$entry_index]);
		}

		$entry_index = 1;
		foreach( $entries_by_email as $entry ) {
			$summarised = $this->summariseDownload($entry, true);
			$summarised['DT_RowId'] = "row_" . $entry_index++;
			$result["aaData"][] = (object) $summarised;

			unset($summarised);
		}

		echo json_encode($result);
		exit;
	}

	public function getAjaxContact() {
		$this->sendOriginHeaders();
		header('Content-Type: application/json');

		$this->checkApiKey();

		$analytics_id = $_REQUEST['analytics_id'];

		$result = array();

		if (isset($analytics_id) && !empty($analytics_id)) {
			$analytics_id = urldecode($analytics_id);
			$analytics_id = preg_split('/,/', $analytics_id);
			$analytics_ids = array();
			foreach ($analytics_id as $analytics_id_element) {
				 $analytics_ids[] = preg_replace('/[^a-z0-9A-Z]/', '', $analytics_id_element);
			}

			$entries = $this->getDownloads($analytics_ids);
			foreach($entries as $entry) {
				$entry['submit_time'] = date_i18n('d F Y G:i:s', floatval($entry['submit_time']));
				$result[] = $entry;
			}
		}

		echo json_encode($result);
		exit;
	}



	private function sendOriginHeaders() {
		// These are now set in .htaccess
		// Under certain conditions (not sure why) they were stripped from here
	}


	private function checkApiKey($provided_key = null) {
		$local_key = get_field('api_key', 'option');
		if (empty($provided_key)) {
			$provided_key =  $_REQUEST['api-key'];
		}
		if ($provided_key != $local_key) {
			echo json_encode(array('error'));
			exit;
		}
	}

	public function getDownloads($analytics_id = null) {
		global $wpdb, $meosc;

		$lots = mred_get_lots();

		/* Commented for patching on 06.06.16
		$sql = "select row_number,
	                   form_name,
	                   field_name,
	                   field_value,
	                   field_order,
	                   submit_time
	            from (
	                   select entries.*,
	                          @row_number := if ( @last_submit_time = entries.submit_time, @row_number, @row_number + 1) as row_number,
	                          @last_submit_time := entries.submit_time as last_submit_time
	                     from {$wpdb->prefix}cf7dbplugin_submits entries
	                          join {$wpdb->prefix}cf7dbplugin_submits withanalytics on entries.submit_time = withanalytics.submit_time and withanalytics.field_name = 'analytics_id'
	                          cross join (select @last_submit_time := 0.0) a
	                          cross join (select @row_number := 0) b
	                    where entries.field_name not in ('Submitted Login', 'Submitted From', 'pdf-download') " .
	                    (empty($analytics_id) ? '' : ' and withanalytics.field_value in ("' . join('","', $analytics_id) . '")' ) .
	            "
	                 order by entries.submit_time, entries.field_order
	                 ) x ";
	    */
		$sql = "SELECT @row_number := if ( @last_submit_time = entries.submit_time, @row_number, @row_number + 1) as row_number, 
 							entries.form_name, entries.field_name, entries.field_value, entries.field_order, entries.submit_time, 
							@last_submit_time := entries.submit_time as last_submit_time 
	                     FROM {$wpdb->prefix}cf7dbplugin_submits entries
	                          join {$wpdb->prefix}cf7dbplugin_submits withanalytics on entries.submit_time = withanalytics.submit_time and withanalytics.field_name = 'analytics_id'
	                          join (select @last_submit_time := 0.0) a
	                          join (select @row_number := 0) b
	                    where entries.field_name not in ('Submitted Login', 'Submitted From', 'pdf-download')" .
	                    (empty($analytics_id) ? '' : ' and withanalytics.field_value in ("' . join('","', $analytics_id) . '")' ) .
	            " order by entries.submit_time, entries.field_order";

		$raw_downloads = $wpdb->get_results($sql);

		$result = array();

		foreach ($raw_downloads as $index => &$raw_download) {
			
			unset($raw_downloads[$index]->last_submit_time);
			
			if (!isset($result[$raw_download->row_number]['contact_type'])) {
				$result[$raw_download->row_number]['contact_type'] = 'contact';
			}
			
			$result[$raw_download->row_number]['form_name'] = $raw_download->form_name;
			$result[$raw_download->row_number]['submit_time'] = $raw_download->submit_time;
			
			$field_name = ( $raw_download->field_name == "sender" ? "email" : $raw_download->field_name );
			
			$result[$raw_download->row_number][$field_name] = $raw_download->field_value;
			
			if ($field_name == "file_id" && !empty($meosc)) {
				
				$file_id = $meosc->decodeAttachmentId((int) $raw_download->field_value);
				
				$result[$raw_download->row_number][$field_name] = $file_id;
				$result[$raw_download->row_number]['contact_type'] = 'file';

				unset($file_id);
			}
			else if ($field_name == "post_id") {
				
				if (!empty($lots[$raw_download->field_value])) {
					
					$result[$raw_download->row_number]["lot_code"] = $lots[$raw_download->field_value]['name'];
				}
				else {
					$post_details = get_post($raw_download->field_value);
					$result[$raw_download->row_number]["lot_code"] = $post_details->post_title;
				}
			}
			
			unset($raw_downloads[$index]);
		}

		return $result;
	}

	public function getAjaxDevelopment() {
		$this->sendOriginHeaders();
		header('Content-Type: application/json');
		$this->checkApiKey();

		echo json_encode(array(
			'lots' => mred_get_lots(array('pdf', 'floor_position_image', 'floor_position_image_fallback', 'lot_situation_image', 'lot_situation_image_fallback')),
			'plans' => mred_get_plans(array('pdf')),
			'floors' => mred_get_floors(array('plan')),
			'buildings' => mred_get_buildings(),
			'sectors' => mred_get_sectors(),
			'lot_types' => mred_get_lot_types(),
			'entries' => mred_get_entries(),
			'translations' => array(
				'rooms' => mred_translate('Rooms'),
				'apartment' => mred_translate('Apartment'),
				'statuses' => mred_get_availability_descriptions()
			)
		));
		exit;
	}

	public function getAjaxStatuses() {
		$this->sendOriginHeaders();
		header('Content-Type: application/json');

		echo json_encode(array(
			'statuses' => mred_get_availability_descriptions()
		));
		exit;
	}


	public function setAjaxStatus() {
		$this->sendOriginHeaders();
		header('Content-Type: application/json');
		$this->checkApiKey();

		$lot_id = (int) $_POST['lot_id'];
		$lot = mred_get_lot($lot_id);
		if (empty($lot)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __('Invalid lot ID', 'meo_real_estate_admin')
			));
			exit;
		}

		$valid_statuses = mred_get_availability_descriptions();
		if (!array_key_exists($_POST['status'], $valid_statuses)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __('Invalid status', 'meo_real_estate_admin')
			));
			exit;
		}

		update_post_meta($lot_id, 'availability', $_POST['status']);

		echo json_encode(array(
			'success' => 1,
			'message' => __('Status updated', 'meo_real_estate_admin')
		));
		
		// PDF Generation
		if(function_exists('mmpg_fpdf_generate')){
			mmpg_fpdf_generate();
		}
		
		// JS/Script Generation
		if(function_exists('mmpg_generate_scripts_callback')){
			mmpg_generate_scripts_callback();
		}
		
		exit;
	}
	

	public function setAjaxPrice() {
		$this->sendOriginHeaders();
		header('Content-Type: application/json');
		$this->checkApiKey();

		$lot_id = (int) $_POST['lot_id'];
		$lot = mred_get_lot($lot_id);
		
		if (empty($lot)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __('Invalid lot ID', 'meo_real_estate_admin')
			));
			exit;
		}
		
		$price = $_POST['price'];

		update_post_meta($lot_id, 'price', $price);

		echo json_encode(array(
			'success' => 1,
			'message' => __('Price updated', 'meo_real_estate_admin')
		));
		
		// PDF Generation
		if(function_exists('mmpg_fpdf_generate')){
			mmpg_fpdf_generate();
		}
		
		// JS/Script Generation
		if(function_exists('mmpg_generate_scripts_callback')){
			mmpg_generate_scripts_callback();
		}
		
		exit;
	}



	private function summariseDownload($entry, $address_join = false) {
		$result = array(
			"name"         => join( ", ", array($entry['surname'], $entry['first_name'])),
			"language"     => isset($entry['language']) && $entry['language'] ? $entry['language'] : 'fr',
			"lots"         => $entry['lot_code'],
			"first_contact" => date('Y-m-d H:i:s', $entry['submit_time'])// date('', [] => 1410437471.9392),
		);

		foreach (array('phone', 'analytics_id', 'surname', 'first_name', 'contact_type') as $field) {
			$result[$field] = $entry[$field];
		}

		$full_address = array();
		foreach (array('address', 'city', 'postcode', 'country', 'email') as $field) {
			$result[$field] = $entry[$field];
			if (isset($entry[$field]) && $entry[$field]) {
				if ($address_join) {
					$full_address[] = $entry[$field];
				}
			}
		}

		if ($address_join) {
			$result["full_address"] = join('<br>', $full_address);
		}

		return $result;
	}
}
