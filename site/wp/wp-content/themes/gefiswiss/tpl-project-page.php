<?php
/*
 * Template Name: Tpl Project Page
 */
global $post,
$mk_options;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );

get_header('notitle'); ?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
    <div class="mk-main-wrapper-holder">
		<div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper no-padding-margin-top <?php echo $page_layout; ?>-layout <?php echo $padding; ?> mk-grid vc_row-fluid">
			<div class="theme-content no-padding-margin-top <?php echo $padding; ?>" itemprop="mainContentOfPage">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post();?>
						<?php the_content();?>
						<div class="clearboth"></div>
						<?php wp_link_pages( 'before=<div id="mk-page-links">'.__( 'Pages:', 'mk_framework' ).'&after=</div>' ); ?>
				<?php endwhile; ?>
						<?php
						if($mk_options['pages_comments'] == 'true') {
							if ( comments_open() ) :
							comments_template( '', true ); 	
							endif;
						}
						?>
                                    <!--<div id="filter-realisation" class="filter-realisation">
                                        <p class="title-filter">
                                            <?php echo __('[:fr]Filtrer les projets[:en]Projects filter') ?>                                
                                        </p>
                                        <select id="list-status" name="list-status">
                                            <option value="0" selected="selected"><?php echo __('[:fr]Tous les projets[:en]All projects'); ?></option>
                                            <option value="1"><?php echo __('[:fr]En cours[:en]In progress'); ?></option>
                                            <option value="2"><?php echo __('[:fr]Termin&eacute;s[:en]Finish'); ?></option>
                                        </select>
                                        <div class="clearboth"></div>
                                    </div>-->
				</div>
                                <div id="projects-container">
                <?php
                $args = array(
                    'post_type' => 'projet',
                    'post_parent' => 0,
                    'order' => 'DESC'
                );
                $projects = query_posts($args);
                $count = 0;
                
                // The Loop
                $html = '<div class="row">';
               
                foreach ($projects as $project)
                {
                    if($count >= 3)
                    {
                        $html .= '</div>';
                        $html .= '<div class="row">';
                        $count = 0;
                    }
                    
                    $categories = get_the_terms( $project->ID, 'cat_projet' );
                    
                    foreach ($categories as $category)
                    {
                        $status_category = get_field_object('field_565c0f6a98eea', 'cat_projet_'.$category->term_id); // project status
                        /*
                         * 
                        Array ( 
                        [ID] => 513 
                        [key] => field_565c0f6a98eea 
                        [label] => �tat 
                        [name] => status 
                        [prefix] => 
                        [type] => radio 
                        [value] => En cours [menu_order] => 1 [instructions] => [required] => 1 [id] => [class] => [conditional_logic] => 0 
                        [parent] => 278 
                        [wrapper] => Array ( [width] => [class] => [id] => ) [_name] => status [_input] => [_valid] => 1 
                        [choices] => Array ( [En cours] => En cours [Termin�] => Termin� ) 
                        [other_choice] => 0 [save_other_choice] => 0 [default_value] => En cours [layout] => vertical ) 
                         * 
                         */
                        if($status_category['value'] == 'En cours'){
                            $class = 'global_project status_1';
                        }else if(substr($status_category['value'], 0, 6) == 'Termin'){
                            $class = 'status_2';
                        }else{
                            $class = 'status_0';
                        }
                    } 
                    
                    $featuredImageId = get_post_thumbnail_id($project->ID);        
                    $imageUrl = wp_get_attachment_image_src($featuredImageId, 'full');
                    $style = 'style="background: transparent url('.$imageUrl[0].') no-repeat; background-position: center; background-size: cover;"';
                   	$postType = get_field_object('field_56c444d5f9129', $project->ID); // Project / Product Type
                   	$postType['value'] = str_replace(array('MANDATS', 'PRIV&Eacute;S'), array('MANDAT', 'PRIV&Eacute;'), htmlentities($postType['value']));
                    
                    $html .= '<div class="col-md-4 '.$class.'">';   
                    $html .=    '<article class="bloc-project">';   
                    $html .=        '<div '.$style.' class="vignette-img">';
                    $html .=        '</div>';
                    $html .=        '<div class="vignette">';
                    $html .=            '<h3 class="h3-project">'.htmlentities($status_category['value']).'</h3>';
                    $html .=            '<h2>'.$project->post_title.'</h2>';
                    $html .=            '<h3 class="h3-project-location">'.get_field('localite',$project->ID).'</h3>';
                    $explodedContent = array();
                    $explodedContent = explode(' ', $project->post_content);
                    					
                    $html .=            '<div>';
			                    			for($i=0;$i<=15;$i++){
			                    				if(isset($explodedContent[$i]) && !empty($explodedContent[$i])){
			                    					if(strpos($explodedContent[$i], '[vc_row') !== false) {
			                    						$html .= str_replace('[vc_row', '', $explodedContent[$i]). ' ';
			                    						break;
			                    					}
			                    					$html .= str_replace('<strong>', '', $explodedContent[$i]). ' ';
			                    				}
			                    			}
                    $html .=            ' [...]</div>';
                    			
                    $html .=            '<a href="'.$project->guid.'" title="'.$project->post_title.'">';
                    $html .=                '<input type="button" class="btn btn-default btn-outline-border-color" value="'.__('[:fr]En savoir plus[:en]Read more').'">';
                    $html .=            '</a>';
                    $html .=        '</div>';
                    $html .=    '</article>';
                    $html .= '</div>';
                    
                    $count++;
                }
                $html .= '</div>';
                echo $html;
                // Reset Query
                wp_reset_query(); 
                ?>
                </div>
		<div class="clearboth"></div>
		</div>
		<div class="clearboth"></div>                
	</div>
</div>
<script type="text/javascript">
jQuery( document ).ready(function() {
    
    jQuery('#list-status').change(function(){
        var status = jQuery(this).val();
        var data = {
    			'action': 'project_filter',
    			'status': status
    		};

    		jQuery.post(ajaxurl, data, function(response) {
    			jQuery('#projects-container').html(response);
    		});
    });
});
</script>
<?php get_footer(); ?>