<?php
require_once('class/Mobile_Detect.php');
global $wpdb;
global $post,
$mk_options;

$user_ID = get_current_user_id();
$categories = get_the_terms($post->ID, 'cat_projet');

$main_access_private = false;

$arrayFiles = array();

$actualities = array();
$detect = new Mobile_Detect;
$count = 1;
// Check Si l'utilisateur est lié à une catégorie
if(!empty($user_ID))
{
    // Recupération des liaison entre user et catégorie
    $list_droit_acces = $wpdb->get_results( 'SELECT * FROM wp_access_user_relationship WHERE user_id = '.$user_ID.' AND type="projet"');
    
    //Boucle qui parcours le resultat de la query ci-dessus
    foreach ($list_droit_acces as $result)
    {
        // Boucle qui parcours les catégories lié au post
        if(!empty($categories) && is_array($categories)){
            foreach ($categories as $category)
            {                   
                // Test pour checker si le user est lié à la catégorie
                if($result->term_id == $category->term_id)
                {
                    $main_access_private = true;
                }
            }
        }
    }
}

// Boucle qui parcours les catégories lié au post
foreach ($categories as $category)
{          
    if( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ):
 	// loop through the rows of data
        while ( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ) : the_row();

            $cat_id = $category->term_id;
            $title = get_sub_field('title_documentation');
            $file = get_sub_field('fichier', false);
            $access_file = get_sub_field('acces');

            $arrayFiles[] = [
                'cat_id' => $cat_id,
                'title' => $title,
                'file' => $file,
                'access' => $access_file,
                'permission' => $main_access_private
            ];

        endwhile;
    endif;
}

// Boucle qui parcours les catégories
foreach ($categories as $category)
{
    if(!empty($catQuery))
    {
        $catQuery .= ',';
    }
    $catQuery .= $category->term_id;
}

if(empty($catQuery))
{
    $actualities = null;
}else{
    $args = array(
        'post_type'=> 'post',
        'cat' => $catQuery,
        'order'    => 'ASC'
    );
    $actualities = new WP_Query( $args );    
}

get_header('notitle'); 
?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
    <div class="mk-main-wrapper-holder">
        <div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper mk-grid vc_row-fluid">
            <div class="theme-content" itemprop="mainContentOfPage">
                <div class="wpb_row vc_inner vc_row vc_row-fluid">
                    <div id="project-col-left" class="wpb_column vc_column_container vc_col-sm-8">
                        <?php
                        // The Loop
                        echo '<a href="'.get_site_url().'/?p=2230#filter-realisation" title="retour au réalisation" id="btn-back-rea"><i class="fa fa-angle-left" style="padding-right: 10px"></i>Retour aux réalisations</a>';
                        if($detect->isMobile() && !$detect->isTablet()){ 
                            echo do_shortcode('[mk_fancy_title color="#00869b" size="35" font_weight="300" font_style="normal" txt_transform="uppercase" margin_bottom="10" font_family="none" el_class="remove-margin-top sub-title"]'.$post->post_title.'[/mk_fancy_title]');
                            echo '<p>'.$post->post_content.'</p>';
                            
                            while ( have_posts() ) : the_post();
                                $args = [
                                    'post_type' => 'projet',
                                    'post_parent' => $post->ID,
                                    'order' => 'ASC'
                                ];
                                query_posts($args);
                                $count = 1;
                                $shortcode .= '<ul class="meo-accordion">'; 
                                while ( have_posts() ) : the_post();
                                    if($count == 1)
                                    {
                                        $tab_active = 'tab_active';
                                    }else{
                                        $tab_active = '';
                                    }
                                    $shortcode .= '<li class="meo-accordion-tab '.$tab_active.'">';
                                    $shortcode .=    '<h4>'.$post->post_title.'</h4>';
                                    $shortcode .=    '<div class="tab-container"><div class="tab-body">'.$post->post_content.'</div></div>';
                                    $html .= $post->post_content."<br/><br/>";
                                    $shortcode .= '</li>';
                                    $count++;
                                endwhile;
                                $shortcode .= '</ul>';    
                            endwhile;
                        }else{
                            while ( have_posts() ) : the_post();
                                echo do_shortcode('[mk_fancy_title color="#00869b" size="35" font_weight="300" font_style="normal" txt_transform="uppercase" margin_bottom="10" font_family="none" el_class="remove-margin-top sub-title"]'.$post->post_title.'[/mk_fancy_title]');
                                the_content();
                                $args = [
                                    'post_type' => 'projet',
                                    'post_parent' => $post->ID,
                                    'order' => 'ASC'
                                ];
                                query_posts($args);
                                // The Loop
                                $shortcode = '[vc_tta_tabs]';
                                while ( have_posts() ) : the_post();        
                                    $categories = get_the_terms($post->ID, 'cat_projet');
                                    $access_private = false;  
                                    if(!empty($categories)){
                                        if(!empty($user_ID))
                                        {
                                            //Boucle qui parcours le resultat de la query ci-dessus
                                            foreach ($list_droit_acces as $result)
                                            {
                                                // Boucle qui parcours les catégories lié au post
                                                foreach ($categories as $category)
                                                {
                                                    // Test pour checker si le user est lié à la catégorie
                                                    if($result->term_id == $category->term_id)
                                                    {
                                                        $access_private = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // Boucle qui parcours les catégories lié au post
                                    if(!empty($categories))
                                    {
                                        foreach ($categories as $category)
                                        {          
                                            if( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ):
                                                // loop through the rows of data
                                                while ( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ) : the_row();

                                                    $cat_id = $category->term_id;
                                                    $title = get_sub_field('title_documentation');
                                                    $file = get_sub_field('fichier', false);
                                                    $access_file = get_sub_field('acces');

                                                    $arrayFiles[] = [
                                                        'cat_id' => $cat_id,
                                                        'title' => $title,
                                                        'file' => $file,
                                                        'access' => $access_file,
                                                        'permission' => $access_private
                                                    ];

                                                endwhile;
                                            endif;
                                        }
                                    }
                                    if(get_post_status ( $post->ID ) == 'private'):
                                        if($access_private):
                                            $shortcode .= '[vc_tta_section title="'.$post->post_title.'" tab_id="1447945632-'.$count.'-7"]'.$post->post_content.'[/vc_tta_section]';
                                        endif;
                                    else:
                                        $shortcode .= '[vc_tta_section title="'.$post->post_title.'" tab_id="1447945632-'.$count.'-7"]'.$post->post_content.'[/vc_tta_section]';
                                    endif;     
                                    $count++;
                                endwhile;        
                                if(!empty($actualities->posts) && is_array($actualities->posts)):
                                    $shortcode .= '[vc_tta_section title="'.__('[:fr]Actualité[:en]News').'" tab_id="1447945632-'.$count.'-7"]';
                                    if(!empty($actualities->posts) && is_array($actualities->posts)):
                                        foreach($actualities->posts as $article):
                                            $title = $article->post_title;
                                            $content = $article->post_content; 
                                            $permalink = get_site_url().'/'.$article->post_name;
                                            $shortcode .= '<article>';
                                            $shortcode .= '<h2>'.$title.'</h2>';
                                            $shortcode .= '<div>';
                                            $shortcode .= $content;
                                            $shortcode .= '</div>';
                                            $shortcode .= '</article>';                                              
                                        endforeach;
                                    endif;                                
                                    $shortcode .= '[/vc_tta_section]';
                                endif;        
                                $shortcode .= '[/vc_tta_tabs]';   
                                wp_reset_query();
                            endwhile;
                        }
                        echo do_shortcode($shortcode);
                        //echo $shortcode;
                        wp_reset_query();

                        $contact_type = get_field('contact_type');
                        $target = '_blank';

                        if($contact_type == 'mail') {
                            $email = get_field('contact_mail');
                            if(!empty($email)) {
                                $url = 'mailto:'.$email;
                                $target = '';
                            }
                        }else if($contact_type == 'url'){
                            $url = get_field('contact_url');
                        }

                        if(empty($url)) {
                            $url = '/contact';
                        }


                        ?>
                    </div>
                    <div id="right-col-rea" class="wpb_column vc_column_container vc_col-sm-4">                        
                        <a target="<?php echo $target ?>" href="<?php echo $url; ?>" title="<?php echo __('contactez-nous') ?>">
                            <div class="project-contact project-email">
                                <span><?php echo __('Contactez-nous'); ?></span>
                                <i class="fa fa-envelope"></i>   
                                <div class="clear-both"></div>
                            </div>
                        </a>
                        <!--<a href="callto:+41216138070" title="<?php echo __('numéro de téléphone') ?>">
                            <div class="project-contact project-phone">
                                <span>+41 21 613 80 70</span>
                                <i class="fa fa-phone-square"></i>  
                                <div class="clear-both"></div>
                            </div>
                        </a>-->
                        <?php
                            $link = get_field('lien_site');
                            if(!empty($link))
                            { ?>
                                <a target="_blank" href="<?php echo get_field('lien_site') ?>" title="<?php echo $post->post_title ?>">
                                    <div class="project-contact project-contact-web project-phone">
                                        <span>Site internet du projet</span>
                                        <i class="fa fa-link"></i>  
                                        <div class="clear-both"></div>
                                    </div>
                                </a>
                        <?php    
                        
                            } 
                            
                            $name_data = get_field('nom',$post->ID);
                            $lieu_data = get_field('lieu',$post->ID);
                            $type_data = get_field('type',$post->ID);
                            $altitude_data = get_field('altitude',$post->ID);
                            $construction_data = get_field('construction',$post->ID);
                            $nbrLit_data = get_field('nombre_de_lits',$post->ID);
                            $ctConstruc_data = get_field('coût_de_construction',$post->ID);
                            $phase_data = get_field('phase',$post->ID);
                        ?>
                        <div id="project-other-infos">
                            <?php if(!empty($name_data)): ?>
                            <div class="other-info">
                                <p class="label-info"><?php echo __('[:fr]Produit[:en]Product') ?></p>
                                <p class="content-info"><?php echo $name_data ?></p>
                            </div>
                            <?php endif; ?>
                            <?php if(!empty($lieu_data)): ?>
                            <div class="other-info">
                                <p class="label-info"><?php echo __('[:fr]Lieu[:en]Place') ?></p>
                                <p class="content-info"><?php echo $lieu_data ?></p>
                            </div>
                            <?php endif; ?>
                            <?php if(!empty($altitude_data)): ?>
                            <div class="other-info">
                                <p class="label-info"><?php echo __('[:fr]Altitude[:en]Altitude') ?></p>
                                <p class="content-info"><?php echo $altitude_data; ?></p>
                            </div>
                            <?php endif; ?>
                            <?php if(!empty($type_data)): ?>
                            <div class="other-info">
                                <p class="label-info"><?php echo __('[:fr]Type[:en]Type') ?></p>
                                <p class="content-info"><?php echo $type_data ?></p>
                            </div>
                            <?php endif; ?>
                            <?php if(!empty($construction_data)): ?>
                            <div class="other-info">
                                <p class="label-info"><?php echo __('[:fr]Construction[:en]Building') ?></p>
                                <p class="content-info"><?php echo $construction_data ?></p>
                            </div>
                            <?php endif; ?>
                            <?php if(!empty($nbrLit_data)): ?>
                            <div class="other-info">
                                <p class="label-info"><?php echo __('[:fr]Nombre de lits[:en]Bed number') ?></p>
                                <p class="content-info"><?php echo $nbrLit_data ?></p>
                            </div>
                            <?php endif; ?>
                            <?php if(!empty($ctConstruc_data)): ?>
                            <div class="other-info">
                                <p class="label-info"><?php echo __('[:fr]Coût de construction CFC 1 à 4[:en]Coût de construction CFC 1 à 4') ?></p>
                                <p class="content-info"><?php echo $ctConstruc_data ?></p>
                            </div>
                            <?php endif; ?>
                            <?php if(!empty($phase_data)): ?>
                            <div class="other-info">
                                <p class="label-info"><?php echo __('[:fr]Phase[:en]Status') ?></p>
                                <p class="content-info"><?php echo $phase_data ?></p>
                            </div>
                            <?php endif; ?>
                            <div class="other-info">
                                <p class="label-info"><?php echo __('[:fr]Programme[:en]Programme') ?></p>                                
                                <?php
                                if( have_rows('programme') ):
                                    echo '<ul>';
                                    while ( have_rows('programme') ) : the_row();
                                        echo '<li>';
                                        echo the_sub_field('information');
                                        echo '</li>';
                                    endwhile;
                                    echo '</ul>';
                                endif;
                                ?>
                            </div>
                        </div> 
                        <?php                            
                            $post_categories = get_the_terms( $post->ID, 'cat_projet' );
                            $cats = array();
                            /*echo __('<h2 class="title-other-project sub-title">Documentation</h2>');
                            echo __('<p>Un listing des documents disponibles, que nous ajouterons au fur et à mesure</p>');
                            echo '<ul id="documentations-list">';
                            
                            foreach($arrayFiles as $file):
                                
                                $id_file = ($file['file']*17)+3;
                            
                                $current_url = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
                                $additional_parameters = 'current_url='.$current_url.'';
                            
                                if($file['access'] == 'private'):
                                    if($file['permission']):
                                        echo '<li>';
                                        echo '<p>' . $file['title'] . '</p>';
                                        if($detect->isMobile() && !$detect->isTablet())
                                        {                                                    
                                            echo '<a class="pdf-download-link" href="http://gefiswiss-sicav-test.meomeo.ch/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'&'.$additional_parameters.'">'.__('Télécharger').'</a>';
                                        }else{
                                            echo '<a class="fancybox-iframe pdf-download-link" href="http://gefiswiss-sicav-test.meomeo.ch/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'">'.__('Télécharger').'</a>';
                                        }
                                        echo '<div class="clear-both"></div>';
                                        echo '</li>';
                                    endif;                                       
                                else:
                                    echo '<li>';
                                    echo '<p>' . $file['title'] . '</p>';
                                    if($detect->isMobile() && !$detect->isTablet())
                                    {                                                    
                                        echo '<a class="pdf-download-link" href="http://gefiswiss-sicav-test.meomeo.ch/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'&'.$additional_parameters.'">'.__('Télécharger').'</a>';
                                    }else{
                                        echo '<a class="fancybox-iframe pdf-download-link" href="http://gefiswiss-sicav-test.meomeo.ch/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'">'.__('Télécharger').'</a>';
                                    }
                                    echo '<div class="clear-both"></div>';
                                    echo '</li>';   
                                endif;
                                
                                
                            endforeach;
                            
                            echo '</ul>'; */ ?>                                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php bloginfo('template_directory');?>/../gefiswiss/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory');?>/../gefiswiss/js/jquery.tabbedcontent.min.js"></script>
<script type="text/javascript">
    window.$ = jQuery;
    $(function() {
        
        /*$(window).load(function() {
           $('div.tab-container').hide();
        });*/
        
        $('.meo-accordion-tab h4').click(function(){            
            if($(this).parent().hasClass('tab_active'))
            {                
                $(this).parent().removeClass('tab_active');
                $(this).parent().children('div.tab-container').hide();
            }else{
               $('div.tab-container').each(function(){
                   $(this).parent().removeClass('tab_active');
                   $(this).hide();
               }); 
               $(this).parent().addClass('tab_active');
               $(this).parent().children('div.tab-container').show();
            }            
        });
    });
</script>
<?php get_footer(); ?>