<?php

global $mk_options;

$page_layout = $mk_options['archive_portfolio_layout'];
$loop_style = $mk_options['archive_portfolio_style'];
$column = $mk_options['archive_portfolio_column'];
$pagination_style = $mk_options['archive_portfolio_pagination_style'];
$image_height = $mk_options['archive_portfolio_image_height'];
$year = '';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $searchInput = $_POST['search'];
    $project = $_POST['list-project'];
    $year = $_POST['list-year'];
    $type_media = $_POST['list-media'];
    
    $select = 'SELECT *';
    $from   = 'FROM wp_posts AS p';
    $join   = 'INNER JOIN wp_postmeta AS pm ON pm.post_id = p.ID';
    $where  = 'WHERE p.post_type="portfolio"';
    $group_by = 'GROUP BY p.ID';
    
    if(!empty($searchInput))
    {
        $where .= ' AND (p.post_title LIKE "%'.$searchInput.'%"';
        $where .= ' OR p.post_content LIKE "%'.$searchInput.'%"';
        $where .= ' OR p.ID IN (SELECT pm.post_id FROM wp_postmeta AS pm WHERE pm.meta_key="_custom_page_title" AND pm.meta_value LIKE "%'.$searchInput.'%")';
        $where .= ' OR p.ID IN (SELECT pm.post_id FROM wp_postmeta AS pm WHERE pm.meta_key="_page_introduce_subtitle" AND pm.meta_value LIKE "%'.$searchInput.'%"))';
    }    
    if(!empty($project))
    {
        $where .= ' AND p.ID IN (SELECT pm.post_id FROM wp_postmeta AS pm WHERE pm.meta_key="projet_relation" AND pm.meta_value LIKE "%'.$project.'%")';
    }    
    if(!empty($year))
    {
        $where .= ' AND p.ID IN (SELECT pm.post_id FROM wp_postmeta AS pm WHERE pm.meta_key="date_de_publication" AND pm.meta_value LIKE "%'.$year.'%")';
    }    
    if(!empty($type_media))
    {
        $where .= ' AND p.ID IN (SELECT pm.post_id FROM wp_postmeta AS pm WHERE pm.meta_key="type_de_media" AND pm.meta_value LIKE "%'.$type_media.'%")';
    }
    
    $query = $select.' '.$from.' '.$join.' '.$where.' '.$group_by;  
    $posts = $wpdb->get_results($query); 
    
    $list_posts = '';
    
    foreach($posts as $post){
        if(!empty($list_posts)){ 
            $list_posts .= ',';
        }
        $list_posts .= $post->ID;
    }
    
}

$args = array(
        'post_type'  => 'projet',
        'post_parent'     => 0
);

$posts_array = get_posts( $args );
    
get_header('notitle'); ?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
	<div class="mk-main-wrapper-holder">
		<div class="theme-page-wrapper <?php echo $page_layout; ?>-layout mk-grid vc_row-fluid row-fluid">
			<div class="theme-content  no-padding-margin-top">
				<?php 
                                $post_page = get_post( 2765 );
				$content = __($post_page->post_content);
				echo do_shortcode($content); ?>
                                <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false vc_row-fluid">
                                    <div class="vc_col-sm-12 wpb_column column_container vc_custom_1448892592966 " style="">


                                        <?php if ( have_posts() ) while ( have_posts() ) : the_post();?>
                                                        <?php the_content();?>
                                                        <div class="clearboth"></div>
                                                        <?php wp_link_pages( 'before=<div id="mk-page-links">'.__( 'Pages:', 'mk_framework' ).'&after=</div>' ); ?>
                                        <?php endwhile; ?>

                                        <div class="filter-presse">
                                            <form id="form-filter" name="form-filter" method="post">
                                            <p>
                                                <span id="label-filter">Filtrer les articles</span>
                                                <span id="mobile-hide-filter" onclick="clickFilterPresse()"><i class="fa fa-angle-down"></i></span>
                                            </p>

                                            <div class="group-input">
                                                <i class="fa fa-search"></i>
                                                <input type="text" id="search" name="search" value="<?php echo (isset($searchInput) && !empty($searchInput)) ? $searchInput : '' ?>" placeholder="Rechercher"/>                                                
                                            </div>  
                                            <select id="list-project" name="list-project">
                                                <option value="0" selected="selected"><?php echo __('[:fr]Tous les projets[:en]All projects'); ?></option>
                                                <?php 
                                                    foreach($posts_array as $post_projet){
                                                        $selected = '';
                                                        if(isset($project) && !empty($project))
                                                        {
                                                            $selected = ($project == $post_projet->ID) ? 'selected' : '' ;
                                                        }
                                                        echo '<option value="'.$post_projet->ID.'" '.$selected.'>'.$post_projet->post_title.'</option>';
                                                    }
                                                ?>
                                            </select>
                                            <select id="list-media" name="list-media">
                                                <option value="0" selected="selected"><?php echo __('[:fr]Tous les m&eacute;dias[:en]All medias'); ?></option>
                                                <option value="video" <?php echo (isset($type_media) && ($type_media == 'video')) ? 'selected' : '' ; ?>><?php echo __('[:fr]Vid&eacute;o[:en]Movie'); ?></option>
                                                <option value="article" <?php echo (isset($type_media) && ($type_media == 'article')) ? 'selected' : '' ; ?>><?php echo __('[:fr]Article[:en]Document'); ?></option>
                                                <option value="audio" <?php echo (isset($type_media) && ($type_media == 'audio')) ? 'selected' : '' ; ?>><?php echo __('[:fr]Audio[:en]Sound'); ?></option>
                                            </select>  
                                            <select id="list-year" name="list-year">
                                                <option value="0"><?php echo __('[:fr]Toutes les ann&eacute;es[:en]All years'); ?></option>
                                                <?php
                                                $date = date('Y');
                                                for( $i=2000 ; $i<=$date ; $i++ )
                                                {                                                    
                                                    $selected = ($year == $i) ? 'selected=true' : '' ;
                                                    echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                                                }
                                                ?>
                                            </select>                                                    
                                            <input type="submit" id="searchBtn" name="searchBtn" value="OK"/>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                                <div id="portfolio-global-content">
                                    <?php   
                                    if(isset($list_posts) && !empty($list_posts))
                                    {
                                        echo do_shortcode( '[mk_portfolio posts="'.$list_posts.'" style="'.$loop_style.'" column="'.$column.'" height="'.$image_height.'" pagination_style="'.$pagination_style.'"]' );    
                                    }else{
                                        echo do_shortcode( '[mk_portfolio style="'.$loop_style.'" column="'.$column.'" height="'.$image_height.'" pagination_style="'.$pagination_style.'"]' );
                                    }
                                    ?>
                                </div>
			</div>

		<?php if ( $page_layout != 'full' ) get_sidebar(); ?>
		<div class="clearboth"></div>
		</div>
	</div>	
</div>
<script type="text/javascript">

    window.$ = jQuery;

    $( document ).ready(function() {   

        /*$('#searchBtn').click(function(){   
            
            var searchInput = $('#search').val();
            var project = $('#list-project').val();
            var year = $('#list-year').val();
            var type_media = $('#list-media').val();            
            var loop_style = $('input[name=loop_style]').val();
            var column = $('input[name=column]').val();
            var height = $('input[name=height]').val();
            var pagination_style = $('input[name=pagination_style]').val();
            
            var data = {
                    'action': 'portfolio_filter',
                    'searchInput': searchInput,
                    'project' : project,
                    'year' : year,
                    'type_media' : type_media,
                    'loop_style' : loop_style,
                    'column' : column,
                    'height' : height,
                    'pagination_style' : pagination_style
            };
            $.post(ajaxurl, data, function(response) {
                    console.log(response);
                    $("#portfolio-global-content").html(response);
                    $("#portfolio-global-content").find('article a').each(function(){
                        $(this).addClass('target','_blank');
                    });
            });
        });*/
    });

    function clickFilterPresse()
    {
        if($('#list-project').css('display') == 'none')
        {
            showFilterOnMobile();
        }else{
            hideFilterOnMobile();
        }
    }

    function showFilterOnMobile()
    {
        $('.group-input').css('display','block');
        $('#list-project').css('display','block');
        $('#list-media').css('display','block');
        $('#list-year').css('display','block');
        $('#searchBtn').css('display','block');
    }

    function hideFilterOnMobile()
    {
        $('.group-input').css('display','none');
        $('#list-project').css('display','none');
        $('#list-media').css('display','none');
        $('#list-year').css('display','none');
        $('#searchBtn').css('display','none');    
    }
    
</script>
<?php get_footer(); ?>
