<?php
/*
 * Template Name: Tpl Documents Page
 */
require_once('class/Mobile_Detect.php');
global $post,
$mk_options;
$detect = new Mobile_Detect;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );

get_header('notitle'); ?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
    <div class="mk-main-wrapper-holder">
        <div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper mk-grid vc_row-fluid no-padding-margin-top">
            <div class="theme-content no-padding-margin-top" itemprop="mainContentOfPage">
                <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false vc_row-fluid">
                    <div class="vc_col-sm-12 wpb_column column_container vc_custom_1448892592966 " style="">                        
                        
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post();?>
                                        <?php the_content();?>
                                        <div class="clearboth"></div>
                                        <?php wp_link_pages( 'before=<div id="mk-page-links">'.__( 'Pages:', 'mk_framework' ).'&after=</div>' ); ?>
                        <?php endwhile; ?>
                        
                    </div>
                </div>
                <?php
                $args = [
                    'post_type' => 'document',
                    'post_parent' => 0,
                    'order' => 'ASC'
                ];
                
                $documents = query_posts($args);
                $count = 0;

                /*echo '<pre>';
                print_r($documents);
                echo '</pre>';

                exit();*/

                $current_url = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
                $additional_parameters = 'current_url='.$current_url.'';
                
                if(is_array($documents) && !empty($documents))
                {
                    $html = '<ul>';
                    foreach ($documents as $doc)
                    {
                        $file = get_field('file',$doc->ID);
                        $id_file = ($file->ID*17)+3;
                        $html .= '<li>';
                        $html .=    '<p>';
                        $html .=    $doc->post_title;
                        $html .=    '</p>';
                        if($detect->isMobile() && !$detect->isTablet())
                        {                                                    
                            $html .= '<a class="pdf-download-link" href="http://gefiswiss-sicav-test.meomeo.ch/file-request/?file_id='.$id_file.'&post_id='.$doc->ID.'&'.$additional_parameters.'">'.__('Télécharger').'</a>';
                        }else{
                            $html .= '<a class="fancybox-iframe pdf-download-link" href="http://gefiswiss-sicav-test.meomeo.ch/file-request/?file_id='.$id_file.'&post_id='.$doc->ID.'">'.__('Télécharger').'</a>';
                        }
                        $html .= '</li>';
                    }
                    $html .= '<ul>';
                }
                
                // Reset Query
                wp_reset_query(); 
                ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>