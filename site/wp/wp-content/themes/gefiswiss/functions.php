<?php

/*
 * Add a new role
 */
$role = 'investisseur';
$display_name = __('Investisseur');
$capabilities = array(
    'read' => true,
    'read_private_pages' => true,
    'read_private_posts' => true
);

add_role( $role, $display_name, $capabilities );

/*
 * Add a custom post type 
 */
add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'projet',
        array(
            'labels' => array(
            'name' => __( 'Projets' ),
            'singular_name' => __( 'Projet' )
        ),
        'hierarchical' => true,
        'description' => 'Page projet',
        'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', 'revisions'),
        'taxonomies' => array('cat_projet'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'page'
        )
    );
}

function add_custom_taxonomies_projet() {
  register_taxonomy('cat_projet', 'projet', array(
    'hierarchical' => true,
    'labels' => array(
      'name' => _x( 'Projet Catégories', 'taxonomy general name' ),
      'singular_name' => _x( 'Projet Catégorie', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Projet Catégories' ),
      'all_items' => __( 'All Projet Catégories' ),
      'parent_item' => __( 'Parent Projet Catégorie' ),
      'parent_item_colon' => __( 'Parent Projet Catégorie:' ),
      'edit_item' => __( 'Edit Projet Catégorie' ),
      'update_item' => __( 'Update Projet Catégorie' ),
      'add_new_item' => __( 'Add New Projet Catégorie' ),
      'new_item_name' => __( 'New Projet Catégorie Name' ),
      'menu_name' => __( 'Projet Catégories' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'categories-projet', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
}
add_action( 'init', 'add_custom_taxonomies_projet', 0 );

/*
add_action( 'init', 'create_post_type_presse' );
function create_post_type_product() {
	register_post_type( 'produit',
			array(
					'labels' => array(
							'name' => __( 'Produits' ),
							'singular_name' => __( 'Produit' )
					),
					'hierarchical' => true,
					'description' => 'Page produit',
					'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', 'revisions'),
					'taxonomies' => array('cat_produit'),
					'public' => true,
					'show_ui' => true,
					'show_in_menu' => true,
					'menu_position' => 5,
					'show_in_nav_menus' => true,
					'publicly_queryable' => true,
					'exclude_from_search' => false,
					'has_archive' => true,
					'query_var' => true,
					'can_export' => true,
					'rewrite' => true,
					'capability_type' => 'page'
			)
	);
}
*/

/*
 * Add a custom post type 
 */
add_action( 'init', 'create_temoignage' );
function create_temoignage() {
    register_post_type( 'temoignage',
        array(
            'labels' => array(
            'name' => __( 'Temoignage' ),
            'singular_name' => __( 'Temoignage' )
        ),
        'public' => true
        )
    );
}
/*add_action( 'init', 'create_documents' );
function create_documents() {
    register_post_type( 'document',
        array(
            'labels' => array(
            'name' => __( 'Documents' ),
            'singular_name' => __( 'Document' )
        ),
        'public' => true
        )
    );
}*/
//add_action('user_category_save','insert_data');
/*function insert_data() {
    global $wpdb;
    
    $user_id = 1;
    $term_id = 1;
    
    $table_name = $wpdb->prefix.'access_user_relationship';
    $wpdb->insert( 
        $table_name, 
        array( 
            'user_id' => $user_id, 
            'term_id' => $term_id,
            'type' => ''
        ) 
    );
}*/

/**
 * Register Widget Area.
 *
 */
function toolbar_widgets_init() {

	register_sidebar( array(
		'name' => 'Toolbar area',
		'id' => 'toolbar_area',
		'before_widget' => '<div id="toolbar-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="rounded">',
		'after_title' => '</h2>',
	) );
}
add_action( 'widgets_init', 'toolbar_widgets_init' );

#############################################
/* Configure Advanced Custom Fields plugin */
#############################################
define('ACF_OPTION_PAGE_STUB', 'acf-options-gefiswiss-settings');

if( function_exists('acf_add_options_page') ) {
	// Note - if changing this, the ACF_OPTION_PAGE_STUB will need to change
	acf_add_options_page('Gefiswiss Settings');
}

if( function_exists('register_field_group') ) {
    
	register_field_group(array (
		'key' => 'group_54dccd889a11f',
		'title' => 'Gefiswiss Settings',
		'fields' => array (
			array (
				'key' => 'field_54dccdaa6f49f',
				'label' => 'Logo',
				'name' => 'logo',
				'prefix' => '',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'medium',
				'library' => 'all',
			),
			array (
				'key' => 'field_54dccddc6f4a0',
				'label' => 'Adresse',
				'name' => 'address',
				'prefix' => '',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 4,
				'new_lines' => 'br',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_54dcce096f4a1',
				'label' => 'Phone',
				'name' => 'phone',
				'prefix' => '',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_54dcce126f4a2',
				'label' => 'email',
				'name' => 'email',
				'prefix' => '',
				'type' => 'email',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
			array (
				'key' => 'field_54dcce1b6f4a3',
				'label' => 'Adresse web',
				'name' => 'url',
				'prefix' => '',
				'type' => 'text',
				'instructions' => 'Without http://',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => ACF_OPTION_PAGE_STUB,
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));
}

// [project ids="1,2,3,45,789"]
function project_func( $atts ){
    $html = '';    
    $a = shortcode_atts( array(
        'ids' => '0'
    ), $atts ); 
    $list_id = explode(',', $a['ids']);
    $args = array(
        'post_type' => 'projet',
        'post__in' => $list_id,
        'post_parent' => 0
    );
    $query = new WP_Query($args);
    if ( $query->have_posts() ) :
        while( $query->have_posts() ) : $query->the_post();
            $featuredImageId = get_post_thumbnail_id($query->post->ID);        
            $imageUrl = wp_get_attachment_image_src($featuredImageId, 'full');
            $style = 'style="background: transparent url('.$imageUrl[0].') no-repeat; background-position: center; background-size: cover;"';
            $html .= '<article class="project-shortcode" '.$style.'>';
            $html .=    '<div class="project-align-center">';
            $html .=        '<div class="txt-project">';
            $html .=            '<h2>'.$query->post->post_title.'</h2>';
            $html .=            '<h3>'.get_field('localite',$project->ID).'</h3>';
            $html .=        '</div>';
            $html .=        '<a href="'.$query->post->guid.'" title="'.$query->post->post_title.'"><input type="button" value="'.__('[:fr]En savoir plus[:en]Read more').'"/></a>';
            $html .=    '</div>';
            $html .= '</article>';
        endwhile;
    endif;
    // Reset Query
    wp_reset_query();
    
    return $html;
}
add_shortcode( 'project', 'project_func' );


/* Add a CSS wp-admin.css pour cacher les menu du plugin real-estate */
function my_custom_css() {    
    echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/wp-admin.css" type="text/css" media="all" />';  
}
add_action('admin_head', 'my_custom_css');


function shortcodes_in_cf7( $form ) {
    $form = do_shortcode( $form );
    return $form;
}
add_filter( 'wpcf7_form_elements', 'shortcodes_in_cf7' );

/* Shortcode to generate input hidden [inputPreviousUrl url ]
 * [inputPreviousUrl url=""] */
	add_shortcode( 'inputPreviousUrl', 'inputPreviousUrl_shortcode' );

  	function inputPreviousUrl_shortcode() {
            $url = '';
            if(isset($_GET['current_url']) && !empty($_GET['current_url']))
            {
                $url = 'http://'.$_GET['current_url'];
            }
	    return '<input type="hidden" name="pervious_url" value="'.$url.'">';
	}        
/* Shortcode to generate button cancel for file request page on mobile device [buttonCancelPreviousUrl url ]
 * [buttonCancelPreviousUrl url=""] */
        add_shortcode( 'buttonCancelPreviousUrl', 'buttonCancelPreviousUrl_shortcode' );

  	function buttonCancelPreviousUrl_shortcode() {
            
            $url = '';
            $html = '';
            
            if(isset($_GET['current_url']) && !empty($_GET['current_url']))
            {
                $url = 'http://'.$_GET['current_url'];
                $html = '<a class="wpcf7-button-cancel" href="'.$url.'"><input type="button" value="Annuler" /></a>';
            }
            
            return $html;
	}
        
/* Shortcode to generate button price list [buttonPriceList url]
 * [buttonPriceList url=""] */
        add_shortcode( 'buttonPriceList', 'buttonPriceList_shortcode' );

  	function buttonPriceList_shortcode($atts) {
            
            $a = shortcode_atts( array(
	        'url' => '#'
	    ), $atts );
            
            require_once('class/Mobile_Detect.php');
            $detect = new Mobile_Detect();
            
            if(!$detect->isMobile())
            {
                $class = 'fancybox-iframe btn-download-list-price';
                $url = $a['url'];
            }else{
                $current_url = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
                if($detect->isTablet()){
                    $class = 'fancybox-iframe btn-download-list-price';
                    $url = $a['url'];
                }else{
                    $class = 'btn-download-list-price';
                    $url = $a['url'].'&current_url='.$current_url;
                }
            }    
            
            $html = '<a class="'.$class.'" href="'.$url.'">&nbsp;</a>';
            
            return $html;
	}
        
/* Shortcode to generate a projects slider in footer [projectSlider]
 * [projectSlider] */
        add_shortcode( 'projectSlider', 'project_slider_shortcode' );
        
        function project_slider_shortcode($atts) {            
            global $wp;
            
            $args = array(
                'post_type' => 'projet',
                'post_parent' => 0
            );             
            $projects = query_posts($args);
            
            $html = '';  
            if ( !empty($projects) ) :         
                $count = 0;
                $html .= '<div id="footer-project" class="swiper-container">';
                $html .=    '<div class="swiper-wrapper">'; 
                    foreach($projects as $project):
                        
                        $categories = get_the_terms($project->ID, 'cat_projet');
                        $category = $categories[0];
                        
                        $field = get_field('status', $category->taxonomy.'_'.$category->term_id);
                        if($field == 'En cours')
                        {
                            $featuredImageId = get_post_thumbnail_id($project->ID);        
                            $imageUrl = wp_get_attachment_image_src($featuredImageId, 'full');
                            $html .=    '<div class="swiper-slide"><a href="'.get_bloginfo('home').'/'.$project->post_name.'" title="'.$project->post_title.'">';
                            $html .=        '<img src="'.$imageUrl[0].'" alt="'.$project->post_title.'" />';
                            $html .=    '</a></div>';
                        }
                        
                    endforeach;
                $html .=    '</div>';
                $html .=    '<div class="swiper-button-prev swiper-button-white"></div>';
                $html .=    '<div class="swiper-button-next swiper-button-white"></div>';
                $html .= '</div>';  
                
                $html .= '<script type="text/javascript">';
                $html .=    'window.$ = jQuery;';
                $html .=    'function heightFooterProject(){';
                $html .=        'var height = $("#footer-project").width()/1.777777;';
                $html .=        '$("#footer-project").height(height);';
                $html .=        '$("#footer-project").css("overflow","hidden");';
                $html .=    '}';
                $html .=    '$(document).ready(function(){';  
                $html .=        'heightFooterProject();';
                $html .=    '});';                
                $html .=    '$( window ).resize(function() {';
                $html .=        'heightFooterProject();';
                $html .=    '});'; 
                $html .= '</script>';
            endif;
            
            return $html;
	}
        
        // add_filter('cn_cookie_notice_output','openCookieProject',1,1);
        
        function openCookieProject($output)
        {
            // return $output;
            
            global $post;
            
            // Print disclaimer when visiting these pages
            $allowedPages = array(1476, 2510, 2100, 2307, 1733, 2612, 1279, 1861, 2712);
            if(is_page($allowedPages))
            {
                return $output;
            }
            
            return '';
        }
/* Shortcode to generate a testimonial list[testimonial]
 * [temoignage] */
add_shortcode( 'temoignage', 'temoignage_shortcode' );

function temoignage_shortcode($atts) {

    global $wpdb;
    
    $results = $wpdb->get_results( 'SELECT * FROM wp_posts WHERE post_type = "temoignage" AND post_status = "publish" ORDER BY RAND() LIMIT 1', OBJECT );
    
    foreach($results as $result)
    {
        echo '<div class="testimonial">';
        echo '<p>"'.$result->post_content.'"</p>';
        echo '<p class="signature"><span class="editeur">'.get_field('editeur',$result->ID).'</span><br/>';
        echo '<span class="job">'.get_field('job__compagny',$result->ID).'</span></p>';
        echo '</div>';
    }
}

/* [contact] */
add_shortcode( 'contact', 'contact_shortcode' );

function contact_shortcode($atts) {
    ?>
        <a href="<?php echo get_site_url(); ?>/contact/" title="<?php echo __('contactez-nous') ?>">
            <div class="project-contact project-email">
                <span><?php echo __('contactez-nous'); ?></span>
                <i class="fa fa-envelope"></i>   
                <div class="clear-both"></div>
            </div>
        </a>
        <!--<a href="callto:+41216138070" title="<?php echo __('numéro de téléphone') ?>">
            <div class="project-contact project-phone">
                <span>+41 21 613 80 70</span>
                <i class="fa fa-phone-square"></i>  
                <div class="clear-both"></div>
            </div>
        </a>-->
    <?php
}

/* [quicklink url="" content="" icon=""] */
add_shortcode( 'quicklink', 'quicklink_shortcode' );

function quicklink_shortcode($atts) {
    
    $a = shortcode_atts( array(
        'url' => '#',
        'content' => '',
        'icon' => ''
    ), $atts );
    
    ?>
        <a href="<?php echo $a['url'] ?>" title="<?php echo __($a['content']) ?>">
            <div class="quicklink-page">
                <span><?php echo __($a['content']); ?></span>
                <i class="fa <?php echo $a['icon'] ?>"></i>   
                <div class="clear-both"></div>
            </div>
        </a>
    <?php
}

/* [mk_smart_capture file="" post=""] */
add_shortcode( 'mk_smart_capture', 'button_download_shortcode' );

function button_download_shortcode($atts)
{
    require_once('class/Mobile_Detect.php');
    $detect = new Mobile_Detect();
    
    $a = shortcode_atts( array(
        'file' => 0,
        'post' => 0
    ), $atts );
    
    $file = ($a['file']*17)+3;
    $url = get_site_url().'/file-request/?file_id='.$file.'&post_id='.$a['post'];
    
    $add_class = '';
    
    if(!$detect->isMobile())
    {
        $add_class = 'fancybox-iframe';
    }
    
    
    $html  = '<div class="vc_btn3-container btn-download-communique vc_btn3-right" style="margin-bottom: 0">';
    $html .=    '<a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-icon-right vc_btn3-color-sky '.$add_class.'" target="_blank" title="Réglement de placement" href="'.$url.'">';
    $html .=       'Télécharger';
    $html .=       '<i class="vc_btn3-icon fa fa-file-pdf-o"></i>';
    $html .=    '</a>';
    $html .= '</div>';
    echo $html;
    wp_reset_query();
}

/* Override Portfolio */


/* Project Filters */
add_action( 'wp_ajax_project_filter', 'project_filter' );
add_action( 'wp_ajax_nopriv_project_filter', 'project_filter' );

function project_filter(){
	global $wpdb;
	// Status
	$accepted_status = array(0, 1, 2);
	$status = intval( $_POST['status'] );
	if(!in_array($status, $accepted_status)){wp_die();};

	
	$args = array(	'post_type' => 'projet',
					'post_parent' => 0,
					'order' => 'ASC',
					'post_status' => 'publish'
			 );
	
	$projects = query_posts($args);
	$count = 0;
	
	// The Loop
	$html = '<div class="row">';
	 
	foreach ($projects as $project)
	{
		if($count >= 3)
		{
			$html .= '</div>';
			$html .= '<div class="row">';
			$count = 0;
		}
	
		$categories = get_the_terms( $project->ID, 'cat_projet' );
		
		$requestedStatusProject = false;
		
		foreach ($categories as $category)
		{
			$status_category = get_field_object('field_565c0f6a98eea', 'cat_projet_'.$category->term_id); // project status
			
			switch($status){
				case "1":
					if($status_category['value'] == 'En cours'){$requestedStatusProject = true;}
				break;
				
				case "2":
					if(substr($status_category['value'], 0, 6) == 'Termin'){$requestedStatusProject = true;}
				break;
				
				default:
					$requestedStatusProject = true;
				break;
			}
		}
		
		if($requestedStatusProject){
			$featuredImageId = get_post_thumbnail_id($project->ID);
			$imageUrl = wp_get_attachment_image_src($featuredImageId, 'full');
			$style = 'style="background: transparent url('.$imageUrl[0].') no-repeat; background-position: center; background-size: cover;"';
            $postType = get_field_object('field_56c444d5f9129', $project->ID); // Project / Product Type
		
			$html .= '<div class="col-md-4">';
			$html .=    '<article class="bloc-project">';
			$html .=        '<div '.$style.' class="vignette-img">';
			$html .=        '</div>';
			$html .=        '<div class="vignette">';
            $html .=            '<h3 class="h3-project-location">'.get_field('localite',$project->ID).'</h3>';
			$html .=            '<h2>'.$project->post_title.'</h2>';
            $html .=            '<h3 class="h3-project">'.strtoupper(html_entity_decode($postType['value'])).' | '.htmlentities($status_category['value']).'</h3>';
            /*$html .=            '<p>'.substr(html_entity_decode(htmlentities($project->post_content)),0,101).' [...]</p>';*/
            $explodedContent = array();
            $explodedContent = explode(' ', $project->post_content);
             
            $html .=            '<p>';
	            for($i=0;$i<=15;$i++){
	            	if(strpos($explodedContent[$i], '[vc_row') !== false) {
                    	$html .= str_replace('[vc_row', '', $explodedContent[$i]). ' ';
                    	break;
                    }
                    $html .= $explodedContent[$i]. ' ';
	            }
            $html .=            ' [...]</p>';
			$html .=            '<a href="'.$project->guid.'" title="'.$project->post_title.'">';
			$html .=                '<input type="button" class="btn btn-default btn-outline-border-color" value="'.__('[:fr]En savoir plus[:en]Read more').'">';
			$html .=            '</a>';
			$html .=        '</div>';
			$html .=    '</article>';
			$html .= '</div>';
		
			$count++;
		}
	}
	$html .= '</div>';
	echo $html;
	// Reset Query
	wp_reset_query();
	
	wp_die();
	
}

/* Search Filter */
function searchfilter($query) {
	if ($query->is_search && !is_admin() ) {

		$query->set('post_status', 'publish');

	}
	return $query;
}

/* Revue de presse Filters
add_action( 'wp_ajax_portfolio_filter', 'portfolio_filter' );
add_action( 'wp_ajax_nopriv_portfolio_filter', 'portfolio_filter' );

function portfolio_filter(){    
    
    global $wpdb;
    global $mk_options;
    
    // Search Parameters
    $searchInput = $_POST['searchInput'];
    $project = $_POST['project'];
    $year = $_POST['year'];
    $type_media = $_POST['type_media'];

    $page_layout = $mk_options['archive_portfolio_layout'];
    $loop_style = $mk_options['archive_portfolio_style'];
    $column = $mk_options['archive_portfolio_column'];
    $pagination_style = $mk_options['archive_portfolio_pagination_style'];
    $image_height = $mk_options['archive_portfolio_image_height'];
    
    // Search Query
    $select = 'SELECT p.ID';
    $from   = 'FROM wp_posts AS p';
    $join   = 'INNER JOIN wp_postmeta AS pm ON pm.post_id = p.ID';
    $where  = 'WHERE p.post_type="portfolio"';
    $group_by = 'GROUP BY p.ID';
    
    if(!empty($searchInput))
    {
        $where .= ' AND (p.post_title LIKE "%'.$searchInput.'%"';
        $where .= ' OR p.post_content LIKE "%'.$searchInput.'%"';
        $where .= ' OR p.ID IN (SELECT pm.post_id FROM wp_postmeta AS pm WHERE pm.meta_key="_custom_page_title" AND pm.meta_value LIKE "%'.$searchInput.'%")';
        $where .= ' OR p.ID IN (SELECT pm.post_id FROM wp_postmeta AS pm WHERE pm.meta_key="_page_introduce_subtitle" AND pm.meta_value LIKE "%'.$searchInput.'%"))';
    }    
    if(!empty($project))
    {
        $where .= ' AND p.ID IN (SELECT pm.post_id FROM wp_postmeta AS pm WHERE pm.meta_key="projet_relation" AND pm.meta_value LIKE "%'.$project.'%")';
    }    
    if(!empty($year))
    {
        $where .= ' AND p.ID IN (SELECT pm.post_id FROM wp_postmeta AS pm WHERE pm.meta_key="date_de_publication" AND pm.meta_value LIKE "%'.$year.'%")';
    }    
    if(!empty($type_media))
    {
        $where .= ' AND p.ID IN (SELECT pm.post_id FROM wp_postmeta AS pm WHERE pm.meta_key="type_de_media" AND pm.meta_value LIKE "%'.$type_media.'%")';
    }
    
    $query = $select.' '.$from.' '.$join.' '.$where.' '.$group_by;  
    $posts = $wpdb->get_results($query); 
    
    $list_posts = '';
    
    foreach($posts as $post){
        if(!empty($list_posts)){ 
            $list_posts .= ',';
        }
        $list_posts .= $post->ID;
    }
    
    $loop_style = $mk_options['archive_portfolio_style']; 
    $column = $mk_options['archive_portfolio_column'];
    $pagination_style = $mk_options['archive_portfolio_pagination_style'];
    $image_height = $mk_options['archive_portfolio_image_height'];
    
    wp_reset_query();    
    if(isset($posts) && !empty($posts))
    {
        echo do_shortcode( '[mk_portfolio posts="'.$list_posts.'" style="'.$loop_style.'" column="'.$column.'" height="'.$image_height.'" pagination_style="'.$pagination_style.'"]' );
    }else{
        echo 'Aucun articles disponibles';
    }
    die();
}*/

add_filter( 'page_attributes_dropdown_pages_args', 'so_3538267_enable_drafts_parents' );
add_filter( 'quick_edit_dropdown_pages_args', 'so_3538267_enable_drafts_parents' );

function so_3538267_enable_drafts_parents( $args )
{
    $args['post_status'] = 'draft,publish,pending';
    return $args;
}
